<?php
    include('../custom-includes/init.class.php');
    $init = new init();
    setlocale(LC_ALL, 'nl_NL');

    // Checking for minimum PHP version
    if (version_compare(PHP_VERSION, '5.3.7', '<')) {
        exit("Sorry, Simple PHP Login does not run on a PHP version smaller than 5.3.7 !");
    } else if (version_compare(PHP_VERSION, '5.5.0', '<')) {
        require_once(Setting::baseUrlHttps.'/custom-libraries/password_compatibility_library.php');
    }

    // create a login object. when this object is created, it will do all login/logout stuff automatically
    $account = new account($init->database);
    $viewer = new viewer($init->database);

    // Determine the page target
    $target = 'dashboard';
    if (isset($_GET['page'])) {
        $target = $_GET['page'];
    }

    switch ($target) {
        case "dashboard":
            $textTitle = 'Dashboard | ExamenOverzicht';
            $textMetaDesc = 'Mijn dashboard op ExamenOverzicht.';
            $textHeader = 'Dashboard';
            $imgHeader = Setting::baseUrlHttps.'/custom-images/header-dashboard.png';

            break;    
        case "profiel":
            $textTitle = 'Mijn profiel | ExamenOverzicht';
            $textMetaDesc = 'Mijn profiel op ExamenOverzicht.';
            $textHeader = 'Mijn profiel';
            $imgHeader = Setting::baseUrlHttps.'/custom-images/header-mijn-profiel.png';

            break;
        case "samenvattingen":
            $textTitle = 'Mijn samenvattingen | ExamenOverzicht';
            $textMetaDesc = 'Mijn samenvattingen op ExamenOverzicht.';
            $textHeader = 'Mijn samenvattingen';
            $imgHeader = Setting::baseUrlHttps.'/custom-images/header-mijn-samenvattingen.png';

            break;
        case "bestellingen":
            $textTitle = 'Mijn bestellingen | ExamenOverzicht';
            $textMetaDesc = 'Mijn bestellingen op ExamenOverzicht.';
            $textHeader = 'Mijn bestellingen';
            $imgHeader = Setting::baseUrlHttps.'/custom-images/header-mijn-bestellingen.png';

            break;
    }
?>

<!DOCTYPE html lang="nl">
<head>
    <?php echo '<base href="'.Setting::baseHref.'" />'; ?>
    <meta charset="UTF-8">
    <title><?php echo $textTitle; ?></title>
    <meta name="description" content="<?php echo $textMetaDesc; ?>"/>
    <meta name="author" content="ExamenOverzicht"/> 
    <meta name="robots" content="noindex, nofollow"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="canonical" href="https://www.examenoverzicht.nl/mijn-account" />
    <link rel="icon" type="image/png" href="<?php echo Setting::baseUrlHttps; ?>/favicon.ico"/>
    <script src="<?php echo Setting::baseUrlHttps; ?>/custom-js/jquery-1.11.1.min.js"></script>
    <?php
        // Insert the viewer files only when requested
        if ($account->isUserLoggedIn() == 'ok') {
            if ($target == 'samenvattingen'){ 
                if (isset($_GET['id'])){
                    // Product selected
                    $productId = $_GET['id'];
                    echo '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/viewer.js/0.11.0/crocodoc.viewer.min.css" property="stylesheet">';
                    echo '<script src="https://cdnjs.cloudflare.com/ajax/libs/viewer.js/0.11.0/crocodoc.viewer.min.js"></script>';
                }
            }
        }
    ?>

    <script>
        $(document).ready(function(){
            $.getScript("<?php echo Setting::baseUrlHttps; ?>/custom-js/bootstrap.js");
            $.getScript("<?php echo Setting::baseUrlHttps; ?>/custom-js/mobile-menu.js");

            // Only if a summary is being retrieved (or the overview is shown)
            <?php 
                if ($account->isUserLoggedIn() == 'ok') {
                    if ($target == 'samenvattingen'){   
                    ?>
                    $.getScript("<?php echo Setting::baseUrlHttps; ?>/custom-js/log-device.js");

                    <?php
                        if (isset($_GET['id'])){
                            // Loading summary
                            if (!isset($_GET['viewer'])){
                                // Normal viewer chosen
                            ?>
                            $.getScript("<?php echo Setting::baseUrlHttps; ?>/custom-js/viewer-ajax.js", function() {
                                requestViewer(<?php echo $productId ?>);
                            });
                            <?php
                            }
                            else {
                                if ($_GET['viewer']=='v2'){
                                    // Alternative viewer chosen
                                ?>
                                $.getScript("<?php echo Setting::baseUrlHttps; ?>/custom-js/viewer-ajax.js", function() {
                                    requestViewer2(<?php echo $productId ?>);
                                });
                                <?php
                                }
                            }
                        }
                    }
                }
            ?>
        });
    </script>
</head>

<body>
    <?php 
        if ($account->isUserLoggedIn() == 'ok') {
            if ($target == 'samenvattingen'){   
                if (isset($_GET['id'])){
                    if (isset($_GET['viewer'])){
                        echo '<div class="viewer-div" style="display: none; height: 100%"></div>';
                    }
                }
            }
        }
    ?>

    <div id="overlay"></div>
    <div id="page-container">
        <div id="book-top"></div>
        <div id="book-middle-container">
            <div id="book-left">
                <div id="book-left-inner"></div>
            </div>
            <div id="content-container">
                <div id="logo-course"><a href="<?php echo Setting::baseUrlHttps; ?>/mijn-account"><img src="<?php echo Setting::baseUrlHttps; ?>/custom-images/my-examenoverzicht.png" alt="Mijn ExamenOverzicht" width="504" height="52"></a></div>
                <div id="logo-course-parent"><a href="<?php echo Setting::baseUrlHttps; ?>"><img src="<?php echo Setting::baseUrlHttps; ?>/custom-images/back-to-examenoverzicht.png" alt="Terug naar ExamenOverzicht" width="306" height="38"></a></div>
                <?php // Display the header text ?>
                <div id="account-page-left">
                    <?php
                        if ($account->isUserLoggedIn() == 'ok') {
                        ?>
                        <div class="course-menu-container-switch">
                            <div class="course-menu-top">
                                <img src="<?php echo Setting::baseUrlHttps; ?>/custom-images/header-mijn-account.png" class="header-image" alt="Mijn account">
                            </div>
                            <div class="course-menu-content">
                                <div class="course-menu-item"><a href="<?php echo Setting::baseUrlHttps; ?>/mijn-account"><i class="fa fa-home"></i> Dashboard</a></div>
                                <div class="course-menu-item"><a href="<?php echo Setting::baseUrlHttps; ?>/mijn-account/profiel"><i class="fa fa-child"></i> Mijn profiel</a></div>
                                <div class="course-menu-item"><a href="<?php echo Setting::baseUrlHttps; ?>/mijn-account/bestellingen"><i class="fa fa-shopping-cart"></i> Mijn bestellingen</a></div>
                                <div class="course-menu-item"><a href="<?php echo Setting::baseUrlHttps; ?>/mijn-account/samenvattingen"><i class="fa fa-book"></i> Mijn samenvattingen</a></div>
                                <div class="course-menu-item"><a href="<?php echo Setting::baseUrlHttps; ?>/mijn-account?logout"><i class="fa fa-sign-out"></i> Uitloggen</a></div>
                            </div>
                            <div class="course-menu-footer"></div>
                        </div>
                        <?php
                        }  
                    ?>
                </div>                
                <?php
                    if ($account->isUserLoggedIn() == 'ok') {

                        // Insert the same menu, but this time in mobile style
                    ?>
                    <nav itemtype="http://schema.org/SiteNavigationElement" itemscope="itemscope" role="navigation" class="mobile-menu">
                        <span class="mobile-menu-header">Navigeer naar:</span>
                        <ul class="menu">
                            <li class="menu-item"><a href="<?php echo Setting::baseUrlHttps; ?>/mijn-account"><i class="fa fa-home"></i> Dashboard</a></li>
                            <li class="menu-item"><a href="<?php echo Setting::baseUrlHttps; ?>/mijn-account/profiel"><i class="fa fa-child"></i> Mijn profiel</a></li>
                            <li class="menu-item"><a href="<?php echo Setting::baseUrlHttps; ?>/mijn-account/bestellingen"><i class="fa fa-shopping-cart"></i> Mijn bestellingen</a></li>
                            <li class="menu-item"><a href="<?php echo Setting::baseUrlHttps; ?>/mijn-account/samenvattingen"><i class="fa fa-book"></i> Mijn samenvattingen</a></li>
                            <li class="menu-item"><a href="<?php echo Setting::baseUrlHttps; ?>/mijn-account?logout"><i class="fa fa-sign-out"></i> Uitloggen</a></li>
                        </ul>
                        <div class="responsive-nav-close"></div>
                    </nav>
                    <?php

                        echo '<div id="page-content-container" class="mobile-clear-left">';

                        $errors = null;
                        if ($account->errors) { $errors = $account->errors; }
                        if ($viewer->errors) { $errors = $viewer->errors; }

                        if ($errors) {
                            echo '<div class="alert alert-danger">';
                            echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                            foreach ($errors as $error) 
                            {
                                echo '<strong>Fout: </strong>'.$error.'<br>';
                            }
                            echo '</div>';
                        }

                        $succes = null;
                        if ($account->succes) { $succes = $account->succes; }
                        if ($viewer->succes) { $succes = $viewer->succes; }

                        if ($succes) {
                            echo '<div class="alert alert-success">';
                            echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                            foreach ($succes as $succesItem) 
                            {
                                echo $succesItem.'<br>';
                            }
                            echo '</div>';
                        }

                        echo '<div id="page-content-top"><div class="responsive-nav-icon"></div><h1 class="assistive-element">'.$textHeader.'</h1>';
                        echo '<img src="'.$imgHeader.'" class="header-image" alt="'.$textHeader.'"></div>';
                        echo '<div class="paper-writing">';

                        // Load the page specific content
                        switch ($target) {
                            case "dashboard":

                                /****************************************************
                                *                                                   *
                                * Dashboard
                                *                                                   *
                                ****************************************************/

                                echo '<p>Welkom op je ExamenOverzicht account.</p>';
                                echo '<p>Op deze omgeving kun je het overzicht van <a href="'.Setting::baseUrlHttps.'/mijn-account/bestellingen">je bestellingen</a> zien, nieuwe bestellingen aan je account <a href="'.Setting::baseUrlHttps.'/mijn-account/bestellingen">koppelen</a> en natuurlijk je aangeschafte <a href="'.Setting::baseUrlHttps.'/mijn-account/samenvattingen">digitale samenvattingen bekijken</a>.</p><br>';
                                echo '<p>Nog geen digitale samenvattingen in bezit? Bestel deze dan nu op <a href="'.Setting::baseUrlHttps.'" target="_blank">ExamenOverzicht.nl</a>.</p><br>';
                                break;

                            case "profiel":

                                /****************************************************
                                *                                                   *
                                * Profiel
                                *                                                   *
                                ****************************************************/

                                echo '<p>Ingelogd als: '.$_SESSION['user_name'].'</p>';
                                echo '<p>Gebruikers ID: '.$_SESSION['user_id'].'</p>';
                                echo '<p>Email adres: '.$_SESSION['user_email'].'</p>';
                                echo '<p>Login methode: '.$_SESSION['user_login_method'].'</p>';

                                break;

                            case "bestellingen":

                                /****************************************************
                                *                                                   *
                                * Bestellingen
                                *                                                   *
                                ****************************************************/
                                
                                $productCountTotal = 0;
                                $content = '';
                                $prevOrderId = '';

                                // Get the linked products store v1
                                $orders = $account->getLinkedOrders($_SESSION['user_id']);
                                if ($orders){
                                    foreach ($orders as $order){
                                        $content .= '<br><p><strong>Bestelnummer '.$order["id"].' ('.date("d-m-Y",strtotime($order["created_on"])).')</strong></p>';
                                        $orderClass = new order($init->database);
                                        $orderLines = $orderClass->getOrderLines($order["id"]);
                                        foreach ($orderLines as $orderLine)
                                        {
                                            if ($orderLine["product_format"] == 'hardcopy') $formatName = 'Hardcopy';
                                            if ($orderLine["product_format"] == 'digital') $formatName = 'Digitaal';
                                            if ($orderLine["product_format"] == 'hardcopy + digital') $formatName = 'Hardcopy + Digitaal';

                                            $content .= '<p><i class="fa fa-book"></i> '.$orderLine["product_name"].' ('.$formatName.')</p>';
                                            $productCountTotal++;
                                        }
                                    }
                                }

                                // Get the linked products store v2
                                $products = $account->getLinkedProducts($_SESSION['user_id']);
                                if ($products){
                                    foreach ($products as $product) {
                                        if ($prevOrderId <> $product["order_id"]){
                                            $content .= '<br><p><strong>Bestelnummer '.$product["order_id"].'</strong></p>';
                                        }

                                        if ($product["product_format"] == 'hardcopy') $formatName = 'Hardcopy';
                                        if ($product["product_format"] == 'digital') $formatName = 'Digitaal';
                                        if ($product["product_format"] == 'hardcopy + digital') $formatName = 'Hardcopy + Digitaal';
                                        
                                        $content .= '<p><i class="fa fa-book"></i> '.$product["product_name"].' ('.$formatName.')</p>';
                                        $productCountTotal++;
                                        $prevOrderId = $product["order_id"];
                                    }
                                }

                                if ($productCountTotal == 0) {
                                    echo '<p>Er is nog geen bestelling aan je account gekoppeld.<br>
                                    Koppel je bestelling aan je account om digitale samenvattingen in te kunnen zien of koop een samenvatting op <a href="'.Setting::baseUrlHttps.'" target="_blank">ExamenOverzicht.nl</a><br>
                                    Als je zojuist al een bestelling hebt geplaatst, ontvang je in enkele ogenblikken een code die je hieronder in kunt vullen</p>
                                    ';
                                }
                                else {
                                    echo $content;
                                }

                                echo '<br><p>Koppel je bestelling aan je account:</p>';
                            ?>

                            <form id="link-order-form" role="form" method="post" action="mijn-account/bestellingen">
                                <div class="form-element">
                                    <div class="form-label">Bestelnummer</div>
                                    <input type="text" class="form-text w150" name="id" pattern=".{4,}" required autocomplete="off" />
                                </div>
                                <div class="form-element">
                                    <div class="form-label">Code</div>
                                    <input type="text" class="form-text w150" name="code" pattern=".{6,}" required autocomplete="off" />
                                </div>
                                <div class="form-element">
                                    <input type="hidden" name="form-name" value="link-order" />
                                    <input type="submit" class="btn btn-default form-submit" value="Koppelen" />
                                </div>
                            </form>

                            <?php


                                break;

                            case "samenvattingen":

                                /****************************************************
                                *                                                   *
                                * Samenvattingen
                                *                                                   *
                                ****************************************************/

                                if (!isset($_GET['id'])){
                                    // No specific product selected yet
                                    $productCountDigital = 0;
                                    $productCountTotal = 0;
                                    $content = '';

                                    // Get the linked products store v1
                                    $orders = $account->getLinkedOrders($_SESSION['user_id']);
                                    if ($orders){
                                        $orderClass = new order($init->database);
                                        foreach ($orders as $order){
                                            $orderLines = $orderClass->getOrderLines($order["id"]);
                                            foreach ($orderLines as $orderLine)
                                            {
                                                if (
                                                    ($orderLine["delivery_status"] == 'paid' || $orderLine["delivery_status"] == 'packed' || $orderLine["delivery_status"] == 'shipped' || $orderLine["delivery_status"] == 'promo') && 
                                                    ($orderLine["product_format"] == 'digital' || $orderLine["product_format"] == 'hardcopy + digital')
                                                ){
                                                    $productCountDigital++;
                                                    $content .= '<tr><td>'.$orderLine["product_name"].'</td><td><a href="'.Setting::baseUrlHttps.'/mijn-account/samenvattingen?id='.$orderLine["product"].'">[Viewer 1]</a></td><td><a href="'.Setting::baseUrlHttps.'/mijn-account/samenvattingen?id='.$orderLine["product"].'&viewer=v2">[Viewer 2]</a></td></tr>';
                                                }
                                                $productCountTotal++;
                                            }
                                        }
                                    }

                                    // Get the linked products store v2
                                    $products = $account->getLinkedProducts($_SESSION['user_id']);
                                    if ($products){
                                        foreach ($products as $product) {
                                            if ($product["product_format"] == 'digital' || $product["product_format"] == 'hardcopy + digital') {
                                                $productCountDigital++;
                                                $content .= '<tr><td>'.$product["product_name"].'</td><td><a href="'.Setting::baseUrlHttps.'/mijn-account/samenvattingen?id='.$product["product_id"].'">[Viewer 1]</a></td><td><a href="'.Setting::baseUrlHttps.'/mijn-account/samenvattingen?id='.$product["product_id"].'&viewer=v2">[Viewer 2]</a></td></tr>';
                                            }
                                            $productCountTotal++;
                                        }
                                    }

                                    if ($productCountTotal == 0) {
                                        // No linked products
                                        echo '<p>Er zijn nog geen digitale samenvattingen aan je account gekoppeld.<br>Koppel je bestelling aan je account in <a href="'.Setting::baseUrlHttps.'/mijn-account/bestellingen">mijn account</a> of koop een samenvatting op <a href="'.Setting::baseUrlHttps.'" target="_blank">ExamenOverzicht.nl</a></p>';
                                    }
                                    elseif ($productCountDigital == 0) {
                                        // No linked (digital products)
                                        echo '<p>Je bestelling bevat geen digitale samenvattingen.<br>Koppel je een andere bestelling aan je account in <a href="'.Setting::baseUrlHttps.'/mijn-account/bestellingen">mijn account</a> of koop een samenvatting op <a href="'.Setting::baseUrlHttps.'" target="_blank">ExamenOverzicht.nl</a></p>';
                                    }
                                    else {
                                        // More then 1 digital product
                                        echo '<table class="viewer-links">'. $content .'</table>';
                                    }
                                }
                                else {
                                    // Product selected
                                    if ($account->isUserAuthorized() == 'ok'){
                                        if (!isset($_GET['viewer'])){
                                            echo '<iframe id="viewer-frame" style="min-width: 405px; width: 100%; height: 500px; border-radius: 5px; border: 1px solid #d9d9d9;" allowfullscreen="allowfullscreen"></iframe>';
                                        }
                                        else {
                                            // Alt viewer (inserted above)
                                            echo '<p>Over enkele ogenblikken verschijnt hier de digitale samenvatting, indien dit niet het geval is neem dan contact met ons op via info@examenoverzicht.nl.</p>';
                                        }
                                    }
                                    else {
                                        // Authorization failed
                                        echo '<p>'.$account->isUserAuthorized().'</p>';
                                    }
                                }
                                break;

                        }

                        echo '</div>';
                        echo '<div id="page-content-bottom"></div>'; 
                        echo '</div>';
                    } else {


                        /****************************************************
                        *                                                   *
                        * Not logged in
                        *                                                   *
                        ****************************************************/

                        if ($errors = $account->errors)
                        {
                            echo '<div class="alert alert-danger alert-fit">';
                            echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                            foreach ($errors as $error) 
                            {
                                echo '<strong>Fout: </strong>'.$error.'<br>';
                            }
                            echo '</div>';
                        }

                        if ($succes = $account->succes)
                        {
                            echo '<div class="alert alert-success alert-fit">';
                            echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                            foreach ($succes as $succesItem) 
                            {
                                echo $succesItem.'<br>';
                            }
                            echo '</div>';
                        }

                        echo '<div class="flex-container"><div id="login-container" class="content-area"><h1 class="h1-sm">Log in op ExamenOverzicht</h1><br>';

                        echo '<a class="btn btn-block btn-social btn-lg btn-facebook" href="mijn-account?provider=Facebook"><span class="fa fa-facebook"></span> Log in met Facebook</a>';
                        echo '<a class="btn btn-block btn-social btn-lg btn-instagram" href="mijn-account?provider=Instagram"><span class="fa fa-instagram"></span> Log in met Instagram</a>';
                        echo '<a class="btn btn-block btn-social btn-lg btn-google" href="mijn-account?provider=Google"><span class="fa fa-google"></span> Log in met Google</a>';

                        echo '<br><div class="breakable-line"><span class="in-breakable-line">of</span></div><br>';
                    ?>

                    <!-- login form -->
                    <form id="user-login-form" method="post" action="dashboard">
                        <div class="form-group">
                            <div class="form-label-3">Email</div>
                            <input type="email" class="form-control" name="email" required />
                        </div>
                        <div class="form-group">
                            <div class="form-label-3">Wachtwoord</div>
                            <input type="password" class="form-control" name="password" pattern=".{6,}" required autocomplete="off" />
                        </div>
                        <input type="hidden" name="form-name" value="user-login" />
                        <div class="wide-and-center"><input type="submit" class="btn btn-default" value="Inloggen" /></div>
                    </form>
                    <p>Wachtwoord vergeten? Neem contact met ons op via info@examenoverzicht.nl.</p>

                    <?php
                        echo '</div>';

                        echo '<div id="registration-container" class="content-area"><h1 class="h1-sm">Geen account?<br>Maak er een aan</h1><br>';

                        echo '<a class="btn btn-block btn-social btn-lg btn-facebook" href="mijn-account?provider=Facebook"><span class="fa fa-facebook"></span> Registreer met Facebook</a>';
                        echo '<a class="btn btn-block btn-social btn-lg btn-instagram" href="mijn-account?provider=Instagram"><span class="fa fa-instagram"></span> Registreer met Instagram</a>';
                        echo '<a class="btn btn-block btn-social btn-lg btn-google" href="mijn-account?provider=Google"><span class="fa fa-google"></span> Registreer met Google</a>';

                        echo '<br><div class="breakable-line"><span class="in-breakable-line">of</span></div><br>';
                    ?>

                    <!-- register form -->
                    <form id="user-reg-form" method="post" action="mijn-account">
                        <div class="form-group">
                            <div class="form-label-3">Email</div>
                            <input type="email" class="form-control" name="email" pattern="(?!(^[.-].*|[^@]*[.-]@|.*\.{2,}.*)|^.{254}.)([a-zA-Z0-9!#$%&'*+\/=?^_`{|}~.-]+@)(?!-.*|.*-\.)([a-zA-Z0-9-]{1,63}\.)+[a-zA-Z]{2,15}" required />
                        </div>
                        <div class="form-group">
                            <div class="form-label-3">Naam</div>
                            <input type="text" class="form-control" name="name" required />
                        </div>

                        <div class="form-group">
                            <div class="form-label-3">Wachtwoord (min. 6 tekens lang)</div>
                            <input type="password" class="form-control" name="password" pattern=".{6,}" required autocomplete="off" />
                        </div>
                        <div class="form-group">
                            <div class="form-label-3">Herhaal wachtwoord</div>
                            <input type="password" class="form-control" name="password_repeat" pattern=".{6,}" required autocomplete="off" />
                        </div>
                        <input type="hidden" name="form-name" value="user-reg" />
                        <div class="wide-and-center"><input type="submit" class="btn btn-default" value="Registreren" /></div>
                    </form>

                    <?php
                        echo '</div></div>';
                    }
                ?>

            </div>
            <div id="book-right"></div>
        </div>           
        <div id="book-bottom"></div>
    </div>

    <link rel="stylesheet" href="<?php echo Setting::baseUrlHttps; ?>/custom-css/bootstrap.css?v=<?php echo Setting::cssVersion; ?>" type="text/css" property="stylesheet"/>
    <link rel="stylesheet" href="<?php echo Setting::baseUrlHttps; ?>/custom-css/bootstrap-social.css" type="text/css" property="stylesheet">
    <link rel="stylesheet" href="<?php echo Setting::baseUrlHttps; ?>/custom-css/style.css?v=<?php echo Setting::cssVersion; ?>" type="text/css" property="stylesheet"/>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" property="stylesheet">


    <?php // Insert the analytics code when in the live enviroment
        if(Setting::environmentName=='Live'){ 
            include_once('../custom-includes/analytics.php');    
        } 
    ?>

    </body>
</html>