<?php
namespace Expert\Invoicegen\Model\Resource\Invoicegen;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
public function _construct()
{
    $this->_init('Expert\Invoicegen\Model\Invoicegen', 'Expert\Invoicegen\Model\Resource\Invoicegen');
}

}